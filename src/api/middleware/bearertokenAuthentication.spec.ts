import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiFetchMock from "chai-fetch-mock";
import * as fetchMock from "fetch-mock";
import { AuthenticationApiError } from "../..";
import { BearerTokenAuthenticationMiddleware } from "./bearerTokenAuthentication"
import { Api } from "../api";

chai.use(chaiAsPromised);
chai.use(chaiFetchMock);
const expect = chai.expect;

const fetch = fetchMock.sandbox();


describe("Bearer Token Authentication Middleware", () => {

    describe("Authentication Attempts", () => {
        it("does not authenticate", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "abcd", refresh: "efgh" }))

            const middleware = new BearerTokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "any", {});

            expect(handled).to.be.false;

            fetch.restore();
        })
    })

    describe("Deuthentication Attempts", () => {

        it("deauthenticates with a username and password", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "abcd", refresh: "efgh" }))

            const middleware = new BearerTokenAuthenticationMiddleware();
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const handled = await middleware.authenticate(api, "password", { "username": "jimbo", "password": "goat" });

            expect(handled).to.be.false;

            const handledDeauth = await middleware.deauthenticate(api);
            expect(handledDeauth).to.be.false;

            fetch.restore();
        })

    })

    describe("Bearer Token Header Injection", () => {

        it("injects a static bearer access token", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "huehuehue" }))

            const middleware = new BearerTokenAuthenticationMiddleware({ token: "my-fixed-bearer-token" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { Authorization: "Bearer my-fixed-bearer-token" } });

            fetch.restore();
        })

        it("injects a dynamic bearer access token", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "huehuehue" }))

            const middleware = new BearerTokenAuthenticationMiddleware({ token: () => "my-dynamic-bearer-token" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { Authorization: "Bearer my-dynamic-bearer-token" } });

            fetch.restore();
        })

        it("injects multiple dynamic bearer access tokens", async () => {
            const tokenValues = ["aaa", "bbb"];

            fetch.post("https://www.example.com/login", JSON.stringify({ access: "huehuehue" }))

            const middleware = new BearerTokenAuthenticationMiddleware({ token: () => tokenValues.shift() });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const firstOptions = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            const secondOptions = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });

            expect(firstOptions).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { Authorization: "Bearer aaa" } });
            expect(secondOptions).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { Authorization: "Bearer bbb" } });

            fetch.restore();
        })

        it("uses a different header", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "huehuehue" }))

            const middleware = new BearerTokenAuthenticationMiddleware({ token: "my-fixed-bearer-token", headerName: "Foobar" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { Foobar: "Bearer my-fixed-bearer-token" } });

            fetch.restore();
        })


        it("uses a different token prefix", async () => {
            fetch.post("https://www.example.com/login", JSON.stringify({ access: "huehuehue" }))

            const middleware = new BearerTokenAuthenticationMiddleware({ token: "my-fixed-bearer-token", headerPrefix: "SuperSecret" });
            const api = new Api({ fetch, baseUrl: "https://www.example.com/" }, [middleware])

            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: { Authorization: "SuperSecret my-fixed-bearer-token" } });

            fetch.restore();
        })
    })

})