import { AuthenticationApiError } from "../..";
import { ApiError, CrudApi, GenericDict, RequestOptions, TokenAuthenticationMiddlewareOptions } from "../types";
import { BaseMiddleware } from "./base";


/**
 * Token Authentication Middleware.
 * 
 * Supplies an authorization header with every request which includes a bearer token.
 * The token can be set by running authenticate() with:
 * - type="password"; payload={username, password}
 * - type="token"; payload={access, refresh?, expiresAt?}
 * - type="key"; payload={key}
 */
export class TokenAuthenticationMiddleware extends BaseMiddleware {

    /**
     * The current options in use by this middleware instance.
     */
    options: TokenAuthenticationMiddlewareOptions = {}

    /**
     * The set of default options supplied to this middleware instance.
     */
    defaultOptions: TokenAuthenticationMiddlewareOptions = {
        passwordLoginPath: "login",
        passwordLoginAccessTokenKey: "access",
        passwordLoginRefreshTokenKey: "refresh",
        passwordLoginUsernameKey: "username",
        passwordLoginPasswordKey: "password",
        refreshTokenPath: "token/refresh",
        refreshTokenRefreshTokenKey: "refresh",
        refreshTokenAccessTokenKey: "access",
        refreshTokenExpiresAtKey: undefined,
        refreshBufferSeconds: 10.0,
    }

    /**
     * The Bearer Token to supply when performing API requests.
     * This might be an access token or an API key depending on
     * the type of token authentication being used.
     */
    protected _bearerToken?: string

    /**
     * The refresh token to supply when requesting a new
     * access token.
     */
    protected _refreshToken?: string

    /**
     * The
     */
    protected _expiresAt?: number

    /**
     * The Bearer Token to supply when performing API requests.
     * This might be an access token or an API key depending on
     * the type of token authentication being used.
     */
    get bearerToken(): string | undefined {
        return this._bearerToken
    }

    /**
     * The refresh token to supply when requesting a new
     * access token.
     */
    get refreshToken(): string | undefined {
        return this._refreshToken
    }

    get expiresAt(): number | undefined {
        if (typeof this._expiresAt !== "undefined") {
            return this._expiresAt
        }
        if (typeof this._bearerTokenExpiry !== "undefined") {
            return this._bearerTokenExpiry
        }
        return undefined
    }

    /**
     * Create a new Token Authentication Middleware instance with
     * the supplied options.
     * 
     * @param options The options to configure the middleware with.
     */
    constructor(options?: TokenAuthenticationMiddlewareOptions) {
        super()
        this.options = this.configure(options || {})
    }

    /**
     * Update the configuration of this middleware with the
     * supplied options.
     * 
     * @param options The options to update for this middleware.
     * @returns The new configuration options for this middleware.
     */
    public configure(options: TokenAuthenticationMiddlewareOptions): TokenAuthenticationMiddlewareOptions {
        this.options = {
            ...this.defaultOptions,
            ...this.options,
            ...options
        }
        return this.options
    }

    async authenticate(api: CrudApi, type: string, payload?: { [key: string]: unknown }): Promise<boolean> {
        switch (type) {
            case "password": {
                try {
                    const responseData = await api.fetch<GenericDict, GenericDict>(
                        "POST",
                        this.options.passwordLoginPath as string,
                        undefined,
                        {
                            [this.options.passwordLoginUsernameKey as string]: (payload?.username ? (payload.username as string) : null),
                            [this.options.passwordLoginPasswordKey as string]: (payload?.password ? (payload.password as string) : null),
                        },
                        { authenticating: true }
                    )
                    this._bearerToken = responseData[this.options.passwordLoginAccessTokenKey as string] as string | undefined
                    this._refreshToken = responseData[this.options.passwordLoginRefreshTokenKey as string] as string | undefined
                    return true;
                } catch (e: ApiError | unknown) {
                    const err = e as ApiError
                    throw new AuthenticationApiError(`Authentication attempt failed: ${err.message}`, err.data, err.meta)
                }
            }
            case "token": {
                this._bearerToken = payload?.access ? (payload.access as string) : undefined
                this._refreshToken = payload?.refresh ? (payload.refresh as string) : undefined
                this._expiresAt = payload?.expiresAt ? (payload.expiresAt as number) : undefined
                return true;
            }

            case "key": {
                this._bearerToken = payload?.key ? (payload.key as string) : undefined
                this._refreshToken = undefined
                return true;
            }

            default:
                return false;
        }
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async deauthenticate(_api: CrudApi): Promise<boolean> {
        if (this._bearerToken || this._refreshToken) {
            this._bearerToken = undefined
            this._refreshToken = undefined
            this._expiresAt = undefined
            return true
        }
        return false
    }

    async beforeFetch(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        // If authentication is in progress, DO NOTHING.
        if (options.meta?.authenticating) {
            return await super.beforeFetch(api, options);
        }

        await this.refresh(api);
        if (this._bearerToken) {
            options.headers["authorization"] = `Bearer ${this._bearerToken}`;
        }
        return await super.beforeFetch(api, options);
    }

    /**
     * If possible, extract the token expiry stamp from the
     * current bearer token.
     */
    get _bearerTokenExpiry(): number | undefined {
        if (typeof this._bearerToken === "undefined") {
            return undefined
        }
        try {
            const tokenParts = this._bearerToken.split(".");
            const payloadPart = tokenParts[1] || "";
            const payload = JSON.parse(Buffer.from(payloadPart, "base64").toString("utf-8"));
            const exp = payload["exp"]
            return exp
        } catch (e: Error | unknown) {
            return undefined;
        }
    }

    /**
     * Determine whether or not the current access token is due for
     * renewal or not.
     * 
     * If the bearer token is currently undefined, null, or empty, then
     * the token is considered due for renewal. This allows you to supply
     * an initial refresh token, and then immediately request a fresh
     * access token.
     * 
     * If the bearer token is not a valid access token, does not contain
     * an `exp` expiry key, is not expired, or can not otherwise be
     * processed in an appropriate way, then this property will always
     * return `false`.
     * 
     * @returns `true` if the bearer token is not set, or the bearer token
     * is an access token and it has expired or is about to expire.
     * `false` if the bearer token is supplied but it cannot be interpreted
     * as an access token with an expiry property.
     */
    get isDueForRenewal(): boolean {
        if (typeof (this._bearerToken) === "undefined" || this._bearerToken === null || this._bearerToken === "") {
            return true;
        }

        const now = Math.round(Date.now() / 1000.0);
        const expiryBuffer = (this.options.refreshBufferSeconds || 0);
        const expiresAt = this.expiresAt
        return typeof expiresAt !== "undefined" && now >= (expiresAt - expiryBuffer);
    }

    /**
     * Determine whether or not a token renewal can take place or not.
     * 
     * This is currently determined by the presence of a refresh token.
     * 
     * @returns `true` if a refresh token is available for use, or
     * `false` otherwise.
     */
    get canRenew(): boolean {
        return Boolean(this._refreshToken)
    }

    /**
     * Attempt to refresh the access token.
     * 
     * This method performs all required checks to determine if the token
     * is due for renewal, and if the token can be renewed. If any of
     * these checks return a negative result then no attempt is made to
     * refresh the token.
     * 
     * @param api The {@link CrudApi} instance involved in the current
     * request.
     */
    public async refresh(api: CrudApi): Promise<void> {
        const dueForRenewal = this.isDueForRenewal;
        if (dueForRenewal && this.canRenew) {
            const uri = `${api.baseUrl}${this.options.refreshTokenPath}`;
            const response = await api.fetchMethod(
                uri,
                {
                    method: "POST",
                    headers: { "content-type": "application/json", "accept": "application/json" },
                    body: JSON.stringify({
                        [this.options.refreshTokenRefreshTokenKey as string]: this._refreshToken,
                    })
                }
            )
            const responseData = await response.json() as { [key: string]: unknown };
            if (!response.ok) {
                throw new AuthenticationApiError(`Failed to refresh token: ${response.statusText}`, responseData, { response });
            }
            this._bearerToken = responseData[this.options.refreshTokenAccessTokenKey as string] as string | undefined;
            if (this.options.refreshTokenExpiresAtKey && (this.options.refreshTokenExpiresAtKey as string) in responseData) {
                this._expiresAt = responseData[this.options.refreshTokenExpiresAtKey as string] as number | undefined;
            } else {
                this._expiresAt = undefined
            }
        }
    }

}