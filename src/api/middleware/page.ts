import { CrudApi, PageMiddlewareOptions, RequestOptions } from "../types";
import { BaseMiddleware } from "./base";



/**
 * Page Middlware.
 * 
 * Performs tweaks to paginated requests.
 */
export class PageMiddleware extends BaseMiddleware {

    /**
    * The current options in use by this middleware instance.
    */
    options: PageMiddlewareOptions = {}

    /**
     * The set of default options supplied to this middleware instance.
     */
    defaultOptions: PageMiddlewareOptions = {
        mode: "page",
        pageSize: 20,
        pageParameterName: "page",
        offsetParameterName: "offset",
        pageSizeParameterName: "count",
        mapResult: undefined,
    }

    /**
     * Create a new Token Authentication Middleware instance with
     * the supplied options.
     * 
     * @param options The options to configure the middleware with.
     */
    constructor(options?: PageMiddlewareOptions) {
        super()
        this.options = this.configure(options || {})
    }

    /**
     * Update the configuration of this middleware with the
     * supplied options.
     * 
     * @param options The options to update for this middleware.
     * @returns The new configuration options for this middleware.
     */
    public configure(options: PageMiddlewareOptions): PageMiddlewareOptions {
        this.options = {
            ...this.defaultOptions,
            ...this.options,
            ...options
        }
        return this.options
    }

    async beforeFetch(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        switch (this.options.mode) {
            case "off": {
                break;
            }

            case "page": {
                if (options.queryParameters?.has("page")) {
                    const page = options.queryParameters.get("page") as string;
                    options.queryParameters.delete("page");
                    const pageSize = (this.options.pageSize as number).toString()
                    options.queryParameters.set(this.options.pageParameterName as string, page);
                    if (this.options.usePageSize) {
                        options.queryParameters.set(this.options.pageSizeParameterName as string, pageSize);
                    }
                }
                break;
            }

            case "offset": {
                if (options.queryParameters?.has("page")) {
                    const page = options.queryParameters.get("page") as string;
                    options.queryParameters.delete("page");
                    const pageSize = (this.options.pageSize as number).toString()
                    const offset = ((this.options.pageSize as number) - 1) * parseInt(page);
                    options.queryParameters.set(this.options.offsetParameterName as string, offset.toString());
                    options.queryParameters.set(this.options.pageSizeParameterName as string, pageSize);
                }
                break;
            }

        }

        return await super.beforeFetch(api, options);
    }

    async afterFetch<ResponsePayloadType>(api: CrudApi, options: RequestOptions, response: Response, result: ResponsePayloadType | unknown): Promise<ResponsePayloadType | unknown> {

        switch (this.options.mode) {
            case "off": {
                return result;
            }

            case "page": {
                if (options.queryParameters?.has(this.options.pageParameterName as string)) {
                    if (this.options.mapResult) {
                        result = await this.options.mapResult(result, this, api);
                    }
                }
                return result;
            }

            case "offset": {
                if (options.queryParameters?.has(this.options.offsetParameterName as string)) {
                    if (this.options.mapResult) {
                        result = await this.options.mapResult(result, this, api);
                    }
                }
                return result;
            }

            default: {
                return result;
            }
        }
    }

}