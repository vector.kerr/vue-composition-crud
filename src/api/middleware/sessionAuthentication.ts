import { AuthenticationApiError } from "../..";
import { ApiError, CrudApi, GenericDict, SessionAuthenticationMiddlewareOptions } from "../types";
import { BaseMiddleware } from "./base";



/**
 * Session Authentication Middleware.
 * 
 * Uses session cookies to perform authentication.
 * The session can be set by running authenticate() with:
 * - type="password"; payload={username, password}
 */
export class SessionAuthenticationMiddleware extends BaseMiddleware {

    /**
     * The current options in use by this middleware instance.
     */
    options: SessionAuthenticationMiddlewareOptions = {}

    /**
     * The set of default options supplied to this middleware instance.
     */
    defaultOptions: SessionAuthenticationMiddlewareOptions = {
        passwordLoginPath: "login",
        passwordLoginUsernameKey: "username",
        passwordLoginPasswordKey: "password",
        passwordLogoutPath: "logout",
    }

    /**
     * Create a new Session Authentication Middleware instance with
     * the supplied options.
     * 
     * @param options The options to configure the middleware with.
     */
    constructor(options?: SessionAuthenticationMiddlewareOptions) {
        super()
        this.options = this.configure(options || {})
    }

    /**
     * Update the configuration of this middleware with the
     * supplied options.
     * 
     * @param options The options to update for this middleware.
     * @returns The new configuration options for this middleware.
     */
    public configure(options: SessionAuthenticationMiddlewareOptions): SessionAuthenticationMiddlewareOptions {
        this.options = {
            ...this.defaultOptions,
            ...this.options,
            ...options
        }
        return this.options
    }

    async authenticate(api: CrudApi, type: string, payload?: { [key: string]: unknown }): Promise<boolean> {
        switch (type) {
            case "password": {
                try {
                    await api.fetch<GenericDict, GenericDict>(
                        "POST",
                        this.options.passwordLoginPath as string,
                        undefined,
                        {
                            [this.options.passwordLoginUsernameKey as string]: (payload?.username ? (payload.username as string) : null),
                            [this.options.passwordLoginPasswordKey as string]: (payload?.password ? (payload.password as string) : null),
                        },
                        { authenticating: true }
                    )
                    return true;
                } catch (e: ApiError | unknown) {
                    const err = e as ApiError
                    throw new AuthenticationApiError(`Authentication attempt failed: ${err.message}`, err.data, err.meta)
                }
            }

            default:
                return false;
        }
    }

    async deauthenticate(api: CrudApi): Promise<boolean> {
        // Perform logout POST request
        try {
            await api.fetch<GenericDict, GenericDict>(
                "POST",
                this.options.passwordLogoutPath as string,
                undefined,
                undefined,
                { deauthenticating: true }
            )
            return true;
        } catch (e: ApiError | unknown) {
            const err = e as ApiError
            throw new AuthenticationApiError(`Deauthentication attempt failed: ${err.message}`, err.data, err.meta)
        }
    }

}