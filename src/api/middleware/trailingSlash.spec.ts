import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { mock, instance } from "ts-mockito";
import { TrailingSlashMiddleware } from "./trailingSlash"
import { CrudApi } from "../types";

chai.use(chaiAsPromised);
const expect = chai.expect;


describe("Trailing Slash Middleware", () => {

    describe("Configuration", () => {
        it("has default options", () => {
            const middleware = new TrailingSlashMiddleware();
            expect(middleware.options).to.deep.equal({
                mode: "present"
            })
        })
    })

    describe("Require a trailing slash", () => {

        it("appends a trailing slash if one is not supplied", async () => {
            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middleware = new TrailingSlashMiddleware({ mode: "present" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub/", method: "STUB", headers: {} });
        })


        it("does not make any changes if a trailing slash is already supplied", async () => {
            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middleware = new TrailingSlashMiddleware({ mode: "present" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub/", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub/", method: "STUB", headers: {} });
        })

    })

    describe("Require no trailing slashes", () => {

        it("removes a trailing slash if one is supplied", async () => {
            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middleware = new TrailingSlashMiddleware({ mode: "absent" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub/", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: {} });
        })

        it("removes all trailing slashes if multiple are supplied", async () => {
            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middleware = new TrailingSlashMiddleware({ mode: "absent" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub////", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: {} });
        })

        it("does not make any changes if a trailing slash is not supplied", async () => {
            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            const middleware = new TrailingSlashMiddleware({ mode: "absent" });
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: {} });
        })

    })

    describe("Invalid Mode", () => {

        it("does nothing with existing trailing slashes if an invalid mode is supplied", async () => {
            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            // Run through JSON parser to hide from typescript evaluator
            const middlewareOptions = JSON.parse(JSON.stringify({ mode: "invalid-mode-value" }));
            const middleware = new TrailingSlashMiddleware(middlewareOptions);
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub/", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub/", method: "STUB", headers: {} });
        })

        it("does nothing with missing trailing slashes if an invalid mode is supplied", async () => {
            const mockApi: CrudApi = mock();
            const api = instance(mockApi);

            // Run through JSON parser to hide from typescript evaluator
            const middlewareOptions = JSON.parse(JSON.stringify({ mode: "invalid-mode-value" }));
            const middleware = new TrailingSlashMiddleware(middlewareOptions);
            const options = await middleware.beforeFetch(api, { uri: "https://www.example.com/stub", method: "STUB", headers: {} });
            expect(options).to.be.eql({ uri: "https://www.example.com/stub", method: "STUB", headers: {} });
        })

    })

})