import { CrudApi, BearerTokenAuthenticationMiddlewareOptions, RequestOptions } from "../types";
import { BaseMiddleware } from "./base";



/**
 * Bearer token Authentication Middleware.
 * 
 * Uses a bearer token to authenticate requests.
 */
export class BearerTokenAuthenticationMiddleware extends BaseMiddleware {


    /**
     * The current options in use by this middleware instance.
     */
    options: BearerTokenAuthenticationMiddlewareOptions = {}

    /**
     * The set of default options supplied to this middleware instance.
     */
    defaultOptions: BearerTokenAuthenticationMiddlewareOptions = {
        token: () => "",
        headerName: "Authorization",
        headerPrefix: "Bearer"
    }


    /**
     * Create a new Token Authentication Middleware instance with
     * the supplied options.
     * 
     * @param options The options to configure the middleware with.
     */
    constructor(options?: BearerTokenAuthenticationMiddlewareOptions) {
        super()
        this.options = this.configure(options || {})
    }

    /**
     * Update the configuration of this middleware with the
     * supplied options.
     * 
     * @param options The options to update for this middleware.
     * @returns The new configuration options for this middleware.
     */
    public configure(options: BearerTokenAuthenticationMiddlewareOptions): BearerTokenAuthenticationMiddlewareOptions {
        this.options = {
            ...this.defaultOptions,
            ...this.options,
            ...options
        }
        return this.options
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async authenticate(api: CrudApi, type: string, payload?: { [key: string]: unknown }): Promise<boolean> {
        return false;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async deauthenticate(api: CrudApi): Promise<boolean> {
        return false;
    }

    getToken(): string {
        if (this.options.token instanceof Function) {
            return this.options.token()
        }
        return this.options.token as string
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async beforeFetch(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        options.headers[this.options.headerName || "Authorization"] = this.options.headerPrefix + " " + this.getToken()
        return options;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async afterFetch<ResponsePayloadType>(api: CrudApi, options: RequestOptions, response: Response, result: ResponsePayloadType | unknown): Promise<ResponsePayloadType | unknown> {
        return result;
    }

}