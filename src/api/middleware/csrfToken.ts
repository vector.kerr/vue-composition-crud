import { CrudApi, CsrfTokenMiddlewareOptions, GenericDict, RequestOptions } from "../types";
import { BaseMiddleware } from "./base";
import * as Cookie from "js-cookie";


/**
 * CSRF Token Middleware.
 * 
 * Ensure that a CSRF token is supplied when performing
 * API requests which require them.
 */
export class CsrfTokenMiddleware extends BaseMiddleware {

    options: CsrfTokenMiddlewareOptions = {}

    defaultOptions: CsrfTokenMiddlewareOptions = {
        cookieProvider: Cookie,
        receiveMode: "cookie",
        receiveCsrfHeaderName: "x-csrftoken",
        receiveCsrfHeaderPath: "",
        receiveCsrfCookieName: "csrftoken",
        receiveCsrfCookiePath: "",
        receiveCsrfFunction: async () => undefined,
        sendMode: "header",
        sendCsrfFieldName: "csrf_token",
        sendCsrfHeaderName: "x-csrftoken",
        sendMethodAllowlist: ["POST", "PUT", "PATCH", "DELETE"],
        sendMethodBlocklist: ["GET"],
    }

    protected _csrfToken: string | undefined = undefined

    get csrfToken(): string | undefined {
        return this._csrfToken
    }

    set csrfToken(value: string | undefined) {
        this._csrfToken = value
    }

    /**
     * Create a new CSRF Token Middleware instance with
     * the supplied options.
     * 
     * @param options The options to configure the middleware with.
     */
    constructor(options?: CsrfTokenMiddlewareOptions) {
        super()
        this.options = this.configure(options || {})
    }

    /**
     * Update the configuration of this middleware with the
     * supplied options.
     * 
     * @param options The options to update for this middleware.
     * @returns The new configuration options for this middleware.
     */
    public configure(options: CsrfTokenMiddlewareOptions): CsrfTokenMiddlewareOptions {
        this.options = {
            ...this.defaultOptions,
            ...this.options,
            ...options
        }
        return this.options
    }

    async beforeFetch(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        if (options.meta?.gettingCsrfToken) {
            options.meta.gettingCsrfToken__beforeFetch = true
            return options;
        }

        options = await this.receiveToken(api, options);

        if (this.shouldSend(options.method)) {
            options = await this.sendtoken(api, options);
        }

        return options
    }

    async afterFetch<ResponsePayloadType>(api: CrudApi, options: RequestOptions, response: Response, result: ResponsePayloadType | unknown): Promise<ResponsePayloadType | unknown> {
        if (response.headers.has(this.options.receiveCsrfHeaderName as string)) {
            this._csrfToken = response.headers.get(this.options.receiveCsrfHeaderName as string) as string;
        }
        return result;
    }

    async receiveToken(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        switch (this.options.receiveMode) {
            case "off": {
                break;
            }
            case "header": {
                // Perform a request to retrieve a CSRF token.
                // The response will come through in the afterFetch function, where
                // the response header will be retrieved.
                if (typeof this._csrfToken === "undefined") {
                    await api.fetch(
                        "GET",
                        this.options.receiveCsrfHeaderPath as string,
                        undefined,
                        undefined,
                        { gettingCsrfToken: true }
                    );
                }
                break;
            }
            case "cookie": {
                // Perform a request to retrieve a CSRF token.
                // The will contain a CSRF token cookie, which can
                // then be retrieved with the cookieprovider.
                if (typeof this._csrfToken === "undefined") {
                    await api.fetch(
                        "GET",
                        this.options.receiveCsrfCookiePath as string,
                        undefined,
                        undefined,
                        { gettingCsrfToken: true }
                    );
                }
                this._csrfToken = (this.options.cookieProvider as Cookie.CookiesStatic).get(this.options.receiveCsrfCookieName as string) || this._csrfToken;
                break;
            }
            case "function": {
                this._csrfToken = await (this.options.receiveCsrfFunction as (() => Promise<string | undefined>))() || this._csrfToken;
                break;
            }
            default: {
                console.warn(`The CSRF Token Middleware receive mode '${this.options.receiveMode}' is not supported`);
                break;
            }
        }
        return options
    }

    async sendtoken(api: CrudApi, options: RequestOptions): Promise<RequestOptions> {
        switch (this.options.sendMode) {
            case "off": {
                break;
            }
            case "header": {
                options.headers[this.options.sendCsrfHeaderName as string] = this._csrfToken || ""
                break;
            }
            case "body": {
                if (!options.payload) {
                    options.payload = {}
                }
                (options.payload as GenericDict)[this.options.sendCsrfFieldName as string] = this._csrfToken || "";
                break;
            }
            default: {
                console.warn(`The CSRF Token Middleware send mode '${this.options.sendMode}' is not supported`);
                break;
            }
        }
        return options
    }

    shouldSend(requestMethod: string): boolean {
        if (typeof this.options.sendMethodAllowlist !== "undefined") {
            if (!this.options.sendMethodAllowlist.map(x => x.toUpperCase()).includes(requestMethod.toUpperCase())) {
                return false;
            }
        }
        if (typeof this.options.sendMethodBlocklist !== "undefined") {
            if (this.options.sendMethodBlocklist.map(x => x.toUpperCase()).includes(requestMethod.toUpperCase())) {
                return false;
            }
        }
        return true;
    }

}