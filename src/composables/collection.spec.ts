import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { reset } from "fetch-mock";
import { mock, instance, when, verify, deepEqual } from "ts-mockito";
import { CrudApi } from "../api/types";
import { useCollection } from "./collection";

chai.use(chaiAsPromised);
const expect = chai.expect

interface SimpleItem {
    id?: number;
    name: string;
}


describe("the use collection composable", () => {

    it("should have a blank initial state", (): void => {
        const mockApi: CrudApi = mock();
        const api = instance(mockApi);

        const { isUnloaded, isLoading, isLoaded, isFailed, error, items } = useCollection<SimpleItem>(api, "item");

        expect(isUnloaded.value).to.be.true;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })


    it("should retrieve a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.list<SimpleItem>("item", undefined)).thenResolve([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])

        const { isUnloaded, isLoading, isLoaded, isFailed, error, items, listItems } = useCollection<SimpleItem>(api, "item");

        const promise = listItems();
        await expect(promise).to.eventually.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])

        verify(mockApi.list("item", undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])
    })


    it("should retrieve a collection of filtered items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.list<SimpleItem>("item", deepEqual({ search: "sec", filter: { "id": "2" } }))).thenResolve([
            { id: 2, name: "second" },
        ])

        const { isUnloaded, isLoading, isLoaded, isFailed, error, items, listItems } = useCollection<SimpleItem>(api, "item");

        const promise = listItems({ search: "sec", filter: { "id": "2" } });
        await expect(promise).to.eventually.eql([
            { id: 2, name: "second" },
        ])

        verify(mockApi.list("item", deepEqual({ search: "sec", filter: { "id": "2" } }))).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(items.value).to.eql([
            { id: 2, name: "second" },
        ])
    })


    it("should fail to retrieve a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.list<SimpleItem>("item", undefined)).thenReject(new Error("Mock API Error"))

        const { isUnloaded, isLoading, isLoaded, isFailed, error, items, listItems } = useCollection<SimpleItem>(api, "item");

        const promise = listItems();
        await expect(promise).to.eventually.be.rejectedWith("Mock API Error").and.be.an.instanceOf(Error);

        verify(mockApi.list("item", undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.true;
        expect((error.value as Error).message).to.equal("Mock API Error");
        expect(items.value).to.be.undefined;
    })



    it("should reset after retrieving a collection of items", async () => {
        const mockApi: CrudApi = mock<CrudApi>();
        const api = instance(mockApi);

        when(mockApi.list<SimpleItem>("item", undefined)).thenResolve([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])

        const { isUnloaded, isLoading, isLoaded, isFailed, error, items, reset, listItems } = useCollection<SimpleItem>(api, "item");

        const promise = listItems();
        await expect(promise).to.eventually.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])

        verify(mockApi.list("item", undefined)).called()
        expect(isUnloaded.value).to.be.false;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.true;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(items.value).to.eql([
            { id: 1, name: "first" },
            { id: 2, name: "second" },
            { id: 3, name: "third" }
        ])

        await reset();

        expect(isUnloaded.value).to.be.true;
        expect(isLoading.value).to.be.false;
        expect(isLoaded.value).to.be.false;
        expect(isFailed.value).to.be.false;
        expect(error.value).to.be.undefined;
        expect(items.value).to.be.undefined;
    })

});
