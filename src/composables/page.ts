import { ref } from "vue";
import { CrudApi, Page, QueryOptions } from "../api/types";
import { PageComposable } from "./types";

export const usePage = <ItemType>(api: CrudApi, resource: string): PageComposable<ItemType> => {
    const isUnloaded = ref<boolean>(true);
    const isLoading = ref<boolean>(false);
    const isLoaded = ref<boolean>(false);
    const isFailed = ref<boolean>(false);
    const error = ref<Error | unknown>();
    const page = ref<Page<ItemType> | undefined>();
    const items = ref<Array<ItemType> | undefined>();

    const reset = async (): Promise<void> => {
        isUnloaded.value = true;
        isLoading.value = false;
        isFailed.value = false;
        error.value = undefined;
        isLoaded.value = false;
        items.value = undefined;
        page.value = undefined;
    }

    const listItems = async (pageNumber = 1, queryOptions?: QueryOptions): Promise<Page<ItemType>> => {
        isUnloaded.value = false
        isLoaded.value = false
        page.value = undefined
        items.value = undefined
        isFailed.value = false
        error.value = undefined
        isLoading.value = true
        try {
            page.value = await api.page<ItemType>(resource, pageNumber, queryOptions);
            items.value = page.value.items;
            isLoaded.value = true
            isLoading.value = false
            return page.value
        } catch (e: Error | unknown) {
            error.value = e
            isFailed.value = true
            isLoading.value = false
            throw e
        }
    }

    return {
        isUnloaded,
        isLoading,
        isLoaded,
        isFailed,
        error,
        page,
        items,
        reset,
        listItems,
    }
};
