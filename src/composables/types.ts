import { Ref } from "vue";
import { CrudApi, Page, QueryOptions } from "../api/types";

/**
 * Interface for the `useApi` composable
 */
export interface ApiComposable {
    /**
     * A reference to the API object supplied by the composable
     */
    api: Ref<CrudApi>;
}

export interface CrudComposable {

    /**
     * If true, then there has been no attempt to perform an operation
     * with this resource yet.
     */
    isUnloaded: Ref<boolean>;

    /**
     * If true, then an operation is currently in progress for this resource.
     */
    isLoading: Ref<boolean>;

    /**
     * If true, then the latest operation completed successfully for
     * this resource.
     */
    isLoaded: Ref<boolean>;

    /**
     * If true, then the latest operation failed to complete for this
     * resource.
     */
    isFailed: Ref<boolean>;

    /**
     * Contains the error object for the most recent operation failure
     * for this resource.
     */
    error: Ref<Error | unknown>;

}

/**
 * Interface for the `useResource` composable
 */
export interface ResourceComposable<ItemType> extends CrudComposable {

    /**
     * Contains the most recent data for this resource.
     * 
     * After create, this will contain the data of the new item.
     * After retrieve, this will be set to the retrieved item.
     * After update, this will be set to the updated item.
     * After delete, this will be undefined.
     */
    item: Ref<ItemType | undefined>;

    /**
     * Create a new item for this resource.
     * 
     * @param value The data to use to create the item.
     * @returns the newly created item (supplied by the server).
     */
    createItem(value: ItemType): Promise<ItemType>;

    /**
     * Retrieve an existing item for this resource.
     * 
     * @param id The ID of the item to retrieve.
     * @returns the retrieved item.
     */
    retrieveItem(id: string | number): Promise<ItemType>;

    /**
     * Update an existing item for this resource.
     * 
     * @param id The ID of the item to update.
     * @param value The data to use to update the item.
     * @returns the updated item (supplied by the server).
     */
    updateItem(id: string | number, value: ItemType): Promise<ItemType>;

    /**
     * Delete an existing item for this resource.
     * 
     * @param id The ID of the item to delete.
     */
    deleteItem(id: string | number): Promise<void>;
}

/**
 * Interface for the `useCollection` composable
 */
export interface CollectionComposable<ItemType> extends CrudComposable {
    /**
     * The full set of items retrieved for this collection.
     */
    items: Ref<Array<ItemType> | undefined>;

    /**
     * Reset the collection to its initial state.
     * This clears out all loaded items and state variables.
     */
    reset(): Promise<void>;

    /**
     * Retrieve the list of items for this collection.
     * 
     * @param queryOptions Any query options to supply (for example,
     * search and filter parameters).
     * @param inPlace If set to true, then existing loaded items will
     * not be cleared from the collection while the API query is
     * being performed. This can lead to a smoother visual experience
     * if an update is being made to a collection.
     * @returns The array of items retrieved for this collection.
     */
    listItems(queryOptions?: QueryOptions, inPlace?: boolean): Promise<Array<ItemType>>;
}

/**
 * Interface for the `usePage` composable
 */
export interface PageComposable<ItemType> extends CrudComposable {
    /**
     * The page data for this page.
     * This includes details about the current, previous and next page,
     * the item data for the current page, and any metadata supplied by
     * the server relating to the page.
     */
    page: Ref<Page<ItemType> | undefined>;

    /**
     * The items for the current page.
     */
    items: Ref<Array<ItemType> | undefined>;

    /**
     * Reset the page to its initial state.
     * This clears out all loaded items and state variables.
     */
    reset(): Promise<void>;

    /**
     * Retrieve the requested page of items.
     * 
     * @param page The page to retrieve.
     * @param queryOptions Any query options to supply (for example,
     * search and filter parameters).
     * @returns The page data for this page.
     */
    listItems(page?: number, queryOptions?: QueryOptions): Promise<Page<ItemType>>;
}

/**
 * Interface for the `useInfinitePage` composable
 */
export interface InfinitePageComposable<ItemType> extends CrudComposable {
    /**
     * If true, then this infinite page composable has some items.
     * It is useful to distinguish this from `isLoaded` because when
     * more items are requested, isLoaded will be false while the loading
     * operation is in progress, however because this is an infiniate page
     * component, items are *appended* when `listMoreItems` is called.
     * This variable allows you to persist details on screen by checking
     * against `hasItems` while loading more items in the background and
     * displaying status indications with `isLoading`, `isLoaded`, and
     * `hasError`.
     */
    hasItems: Ref<boolean>;

    /**
     * The page data for the most recently loaded page.
     */
    page: Ref<Page<ItemType> | undefined>;

    /**
     * Set to true if the last page has been reached and there are no
     * more results for this infinite pager to load.
     */
    hasReachedLastPage: Ref<boolean>;

    /**
     * The full set of items that have been retrieved thus far for this
     * infiniate page.
     */
    items: Ref<Array<ItemType> | undefined>;

    /**
     * Reset the infinite page to its initial state.
     * This clears out all loaded items and state variables.
     */
    reset(): Promise<void>;

    /**
     * Request more items for this infinite page.
     * If this is the first request, page 1 will be retrieved.
     * Beyond that, this will use `page.next` to retrieve the
     * next set of relevant results.
     * If there are no more next pages, then this method terminates
     * immediately without performing any further requests.
     * 
     * @param queryOptions Any query options to supply (for example,
     * search and filter parameters).
     * @returns The full set of items that have been retrieved thus
     * far for this infinite page.
     */
    listMoreItems(queryOptions?: QueryOptions): Promise<Array<ItemType>>;
}
