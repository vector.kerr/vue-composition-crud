
## Within a Vue Component

Make use of the various composition functions, including:
* `useApi` - Create API instances
* `useGlobalApi` - Create or retrieve a global API instance
* `useCollection` - Used to retrieve a single collection of items (i.e., response = array)
* `usePage` - Used to retrieve a paginated collection of items (i.e., response = object with page information)
* `useInfinitePage` - Used to retrieve an ever-growing paginated collection of items. Similar to usePage but great for infinite scroll components.
* `useResource` - Used to create, retrieve, update, or delete an individual resource within a collection.

Note that the composition functions generally return `Ref` instances. It is up to you
how you would like to manage this, but if you plan on using the objects directly then
remember that `Ref` instances hide the actual value of a property under `.value`.

For example, if you create an API instance, you would need to use:

```js
// Create the API and get the ref object
const { api } = useApi({baseUrl: "https://www.example.com/"});

// Use the api by accessing the `value` property of the ref object
api.value.get("some-resource");

// Bind a resource to the api using the `value` property of the ref object
const users = useCollection(api.value, "users");
const user = useResource(api,value, "users");
```


### Interactive Example

<iframe src="https://codesandbox.io/embed/green-glade-3ewz4?fontsize=14&hidenavigation=1&theme=dark"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="Vue Composition CRUD Demo"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

[![Vue Composition CRUD Demo](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/green-glade-3ewz4?fontsize=14&hidenavigation=1&theme=dark)



## Supplying the API to Child Components

Using the Vue 3 Provide/Inject system, you can `provide()` a value
from a component, and then receive that value using `inject()` at
a child component of any depth within the original component.

This is often better achieved using a `Symbol` as a constant
reference/identifier when `provide()`ing and `inject()`ing the
value.


### `symbols.js`
```js
export const API = Symbol("api");
```


### `Parent.vue`
```html
<template>
    ...
</template>

<script>
import { provide, readonly } from "vue"
import { useApi } from "vue-composition-crud";
import { API } from "symbols.js";
export default {
    setup() {
        const { api } = useApi();
        provide(API, readonly(api));
    }
}
```

### `TaskList.vue`
```html
<template>
    ...
</template>

<script>
import { inject } from "vue"
import { useCollection } from "vue-composition-crud";
import { API } from "symbols.js";
export default {
    setup() {
        const api = inject(API);
        const tasks = useCollection(api.value, "tasks");
        return {
            api,
            tasks
        };
    }
}
```
