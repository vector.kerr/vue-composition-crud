# On Its Own


```js
// Import the API class
import { Api } from "vue-composition-crud";

// Create a new instance of the API class, and set the
// base url to the root of the API.
const api = new Api({baseUrl: "https://www.example.com/api/"});

// GET https://www.example.com/api/tickets
const tickets = await api.list("tickets");

// GET https://www.example.com/api/attachment/7
const attachment = await api.retrieve("attachments", 7);

// POST https://www.example.com/api/tickets with JSON payload
const newTicket = await api.create("tickets", {title: "New Ticket", body: "This is a new ticket", user: 143});

// PATCH https://www.example.com/api/tickets/<newTicket.id> with JSON payload
const updatedTicket = await api.update("tickets", newTicket.id, {title: "Updated New Ticket Title", completed: true})

// DELETE https://www.example.com/api/tickets/<updatedTicket.id>
await api.delete("tickets", updatedTicket.id);
```
